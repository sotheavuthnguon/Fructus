//
//  FructusApp.swift
//  Fructus
//
//  Created by Sotheavuth Nguon on 7/22/21.
//

import SwiftUI

@main
struct FructusApp: App {
  
  @AppStorage("isOnboarding") var isOnboarding: Bool = true
  
  var body: some Scene {
    WindowGroup {
      if isOnboarding {
        OnboardingView()
      } else {
        ContentView()
      }
    }
  }
}
