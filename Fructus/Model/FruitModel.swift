//
//  FruitModel.swift
//  Fructus
//
//  Created by Sotheavuth Nguon on 8/9/21.
//

import SwiftUI

struct Fruit: Identifiable {
  var id = UUID()
  var title, headline, image: String
  var gradientColors: [Color]
  var description: String
  var nutrition: [String]
}
