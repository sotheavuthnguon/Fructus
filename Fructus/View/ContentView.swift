//
//  ContentView.swift
//  Fructus
//
//  Created by Sotheavuth Nguon on 7/22/21.
//

import SwiftUI

struct ContentView: View {
    
    @State private var isShowingSettings = false
    
    init() {
        UITableView.appearance().showsVerticalScrollIndicator = false
    }
    var body: some View {
        NavigationView {
            List {
                ForEach(fruitsData.shuffled()) { fruit in
                    NavigationLink(
                        destination: FruitDetailView(fruit: fruit),
                        label: {
                            FruitRowView(fruit: fruit)
                                .padding(.vertical, 4)
                        })
                }
            }.navigationTitle("Fruits")
            .navigationBarItems(trailing: Button(action: {
                isShowingSettings = true
            }, label: {
                Image(systemName: "slider.horizontal.3")
            }).sheet(isPresented: $isShowingSettings) {
                SettingView()
            })
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
        .preferredColorScheme(.light)
    
    ContentView()
        .preferredColorScheme(.dark)
  }
}
