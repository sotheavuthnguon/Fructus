//
//  OnboardingView.swift
//  Fructus
//
//  Created by Sotheavuth Nguon on 8/9/21.
//

import SwiftUI

struct OnboardingView: View {
    var body: some View {
      TabView {
        ForEach(fruitsData[0...3]) { fruit in
          FruitCardView(fruit: fruit)
        }
      }
      .tabViewStyle(PageTabViewStyle())
      .padding(.vertical, 20)
    }
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView()
    }
}
